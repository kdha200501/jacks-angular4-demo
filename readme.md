## Introduction

1. demonstrates [`jacks-angular4-cli`](https://www.npmjs.com/package/jacks-angular4-cli) features:

     - in `dev` build, compiles Typescript into ES5 as CommonJs module at build time, and serves the modules through [System Js](https://www.npmjs.com/package/systemjs)
     - in `prod` build, compiles, tree-shakes, bundles, minifies and lazyloads Typescript into ES5 at build time using [Webpack](https://github.com/webpack/webpack)

2. does not demonstrate unit-testing, library build and documentation, will write another demo when there are cycles to spare

## How to install the demo

```
$ git clone https://kdha200501@bitbucket.org/kdha200501/jacks-angular4-demo.git

$ cd jacks-angular4-demo

$ npm install
```

## How to make a `dev` build and watch changes

```
$ npm start
```


## How to make a `dev` build, only

```
$ npm run build-dev
```

## How to make a `prod` build

```
$ npm run build-prod
```
serve up the `prod` build by:

```
$ cd dist
$ python -m SimpleHTTPServer 3000
```

Code Pen [demo page](https://codepen.io/kdha200501/project/full/AekkzK/)