import {Routes, RouterModule} from '@angular/router';
import {AppController} from './controllers/AppController';

export const routes: Routes = [{
    path:      '',
    component: AppController

}];

export const paths = RouterModule.forChild(routes);
