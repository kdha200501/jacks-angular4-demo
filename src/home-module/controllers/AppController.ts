import {Component} from '@angular/core';

@Component({
    templateUrl: '../view/home-module/controllers/app-controller.html',
    styleUrls:   ['../css/home-module/controllers/app-controller.css']
})
export class AppController {
    constructor() {}
}
