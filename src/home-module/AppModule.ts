// dependencies
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
// nested classes
import {AppController} from './controllers/AppController';
import {componentControllerList} from './Manifest';
import {paths} from './paths';

@NgModule({
    declarations: [
        AppController,
        ...componentControllerList
    ],
    imports:      [
        paths,
        RouterModule
    ]
})
export class HomeModule {
}// MVC for the "Page" content-type
