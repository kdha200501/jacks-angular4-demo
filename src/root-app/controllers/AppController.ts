import {Component} from '@angular/core';

@Component({
    selector: 'app-controller',
    template: `
        <router-outlet></router-outlet>
    `
})
export class AppController {
    constructor() {
    }
}
