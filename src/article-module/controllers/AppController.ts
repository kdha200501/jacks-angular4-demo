// dependencies
import {Component, Inject} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
// classes
import {Article, CATEGORY} from '../models/Article';
import {SubmitObservable} from '../../compoment-module/classes/SubmitObservable';
import {InputViewModel} from '../../compoment-module/classes/InputViewModel';
import {ArticleService} from '../services/ArticleService';

@Component({
    templateUrl: '../view/article-module/controllers/app-controller.html',
    styleUrls:   ['../css/article-module/controllers/app-controller.css']
})
export class AppController extends SubmitObservable {
    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
        super();
        this._articleService = ArticleService.getInstance();
        this.resetInput();
        this.loadArticles();
    }

    // Private variables

    private _tmpArticle: Article;
    private _articleList: Array<Article>;
    private _nameFilter: string;
    private _tagFilter: string;
    private _hasError: boolean;
    private _articleService: ArticleService;

    // Private methods

    private loadArticles() {
        let jsonStr = this.storage.get('_articleList');
        if(jsonStr) {
            try{
                this._articleList = JSON.parse( atob(jsonStr) ).map((data: any) => {
                    return new Article(data);
                });
            }
            catch(e) {
                this._articleList = [];
                this.saveArticles();
            }
        }
        else {
            this._articleList = [];
        }
    }

    private saveArticles() {
        let jsonStr = JSON.stringify(
            this._articleList.map((article: Article) => {
                return article.toJson();
            })
        );
        this.storage.set( '_articleList', btoa(jsonStr) );// TODO: move key to config
    }

    private resetInput() {
        this._tmpArticle = new Article({});
        this._nameFilter = '';
        this._tagFilter = '';
        this._hasError = false;
    }

    // Accessors

    public get tmpArticle(): Article {
        return this._tmpArticle;
    }

    public get articleList(): Array<Article> {
        return this._articleList;
    }

    public get nameFilter(): string {
        return this._nameFilter;
    }

    public set nameFilter(value: string) {
        this._nameFilter = value;
    }

    public get tagFilter(): string {
        return this._tagFilter;
    }

    public set tagFilter(value: string) {
        this._tagFilter = value;
    }

    public get categoryList(): Array<string> {
        return Object.keys(CATEGORY);
    }

    public get categoryEnum(): any {
        return CATEGORY;
    }

    // Public methods

    public priorSubmit() {
        this._hasError = false;
    }

    public postSubmit() {
        if(this._hasError) {
            return;
        }
        super.resetError();
        this._articleList = this._articleService.orderArticleByWeight( this._articleList.concat(this._tmpArticle) );
        this.resetInput();
        this.saveArticles();
    }

    public resetArticles() {
        super.resetError();
        this.resetInput();
        this._articleList = [];
        this.saveArticles();
    }

    public validateTmpArticle(inputViewModel: InputViewModel) {
        this._hasError = this._hasError || inputViewModel.hasError;
    }
}
