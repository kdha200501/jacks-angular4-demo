import {Orm} from '../classes/Orm';

export enum CATEGORY {
    JACKALOPE = 'jackalope',// maps machine read-able to human read-able
    LOCHNESS = 'nessy',
    BIGFOOT = 'bigfoot'
}

const CATEGORY_WEIGHT = {
    [CATEGORY.BIGFOOT]: 3,
    [CATEGORY.LOCHNESS]: 2,
    [CATEGORY.JACKALOPE]: 1,
};

interface ArticleArgs {
    name: string;
    tags: string;
    category: string;
    description: string;
}

export class Article extends Orm<ArticleArgs>{
    constructor(data) {
        super(data);
    }

    // Accessors

    public get name(): string {
        return this._model.name;
    }
    public set name(value: string) {
        this._model.name = value;
    }

    public get tags(): string {
        return this._model.tags;
    }
    public set tags(value: string) {
        this._model.tags = value;
    }

    public get category(): string {
        return this._model.category;
    }
    public get categoryWeight(): number {
        return CATEGORY_WEIGHT[CATEGORY[this._model.category]] || 0;
    }
    public set category(value: string) {
        this._model.category = value;
    }

    public get description(): string {
        return this._model.description;
    }
    public set description(value: string) {
        this._model.description = value;
    }
}
