import { Pipe, PipeTransform } from '@angular/core';
import {Article} from '../models/Article';

@Pipe({ name: 'filterByArticleName' })
export class FilterArticleByName implements PipeTransform {
    constructor() {}

    transform(articleList: Array<Article>, nameFilter: string) {
        if(nameFilter) {
            let regexp = new RegExp(nameFilter, 'im');
            return articleList.filter((article) => {
                return regexp.test(article.name);
            })
        }
        else {
            return articleList;
        }
    }
}
