import { Pipe, PipeTransform } from '@angular/core';
import {Article} from '../models/Article';
import {ArticleService} from '../services/ArticleService';

@Pipe({ name: 'orderArticleByWeight' })
export class OrderArticleByWeight implements PipeTransform {
    constructor() {
        this._articleService = ArticleService.getInstance();
    }

    // Private variables
    private _articleService: ArticleService;

    transform(articleList: Array<Article>) {
        return this._articleService.orderArticleByWeight(articleList);
    }
}
