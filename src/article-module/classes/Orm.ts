/** Class to facilitate Object Relational Mapping (ORM) */
export class Orm<T> {
    /**
     * initiates the ORM with a new json object
     *
     * the generic interface T defines the actual mapping to properties in the json object
     *
     * @param {T} model - the json object
     */
    constructor(model: T) {
        this._model = model || <T>{};
    }

    // protected variables
    protected _model: T;

    // public methods
    /**
     * re-initiates the ORM with a new json object
     * @param {T} model - the json object
     */
    public updateModel(model: T): void {
        this._model = model;
    }

    /**
     * returns the json object
     * @returns {T}
     */
    public toJson(): T {
        return this._model;
    }
}
