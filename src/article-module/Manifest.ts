import {Menu} from './components/Menu';
import {FilterArticleByName} from './pipes/FilterArticleByName';
import {FilterArticleByTags} from './pipes/FilterArticleByTags';
import {OrderArticleByWeight} from './pipes/OrderArticleByWeight';

export const componentList = [
    Menu,
    FilterArticleByName,
    FilterArticleByTags,
    OrderArticleByWeight
];
