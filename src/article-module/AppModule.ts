// dependencies
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { StorageServiceModule } from 'angular-webstorage-service';
import {ComponentModule} from '../compoment-module/AppModule';
// nested classes
import {AppController} from './controllers/AppController';
import {componentList} from './Manifest';
import {paths} from './paths';

@NgModule({
    declarations: [
        AppController,
        ...componentList
    ],
    imports:      [
        paths,
        CommonModule,
        FormsModule,
        StorageServiceModule,
        ComponentModule
    ]
})
export class ArticleModule {
}// MVC for the "Page" content-type
