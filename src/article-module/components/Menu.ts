import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector:    'menu',
    templateUrl: '../view/article-module/components/menu.html',
    styleUrls:   ['../css/article-module/components/menu.css']
})
export class Menu {
    @Output() reset: EventEmitter<any> = new EventEmitter();

    constructor() {
        this._isExpand = false;
    }

    // Private variables

    private _isExpand: boolean;

    // Public methods

    public toggle(forceCollapse: boolean) {
        this._isExpand = forceCollapse === true ? false : !this._isExpand;
    }

    // Accessors

    public get isExpand(): boolean {
        return this._isExpand;
    }
}
