import {Article} from '../models/Article';

export class ArticleService {

    // Singleton design pattern
    /**
     * The ArticleService singleton
     * @type {ArticleService}
     * @private
     */
    private static _singleton: ArticleService = null;

    /**
     * Gets the ArticleService singleton.
     * @returns {ArticleService}
     */
    public static getInstance(): ArticleService {
        if (ArticleService._singleton === null) {
            return new ArticleService();
        } else {
            return ArticleService._singleton;
        }
    }

    constructor() {
        if (ArticleService._singleton) {
            throw new Error('Error: Instantiation failed: Use ArticleService.getInstance() instead of new ArticleService().');
            // endIf singleton already instantiated
        } else {
            ArticleService._singleton = this;
            // endIf singleton not yet instantiated
        }
    }

    // Private variables

    public orderArticleByWeight(articleList: Array<Article>):Array<Article> {
        return articleList.sort((a: Article, b:Article) => {
            return b.categoryWeight - a.categoryWeight;
        });
    }
}
