import {SubmitEventObserver} from './SubmitEventObserver';
import {InputViewModel} from './InputViewModel';

export abstract class ValidateOnSubmit extends SubmitEventObserver {
    constructor() {
        super();
        this._showError = false;
    }

    // Private variables

    private _showError: boolean;

    // Accessors

    protected abstract _inputViewModel: InputViewModel;

    public get hasError(): boolean {
        return this._inputViewModel.hasError && this._showError;
    }

    // Public methods

    public abstract validateViewModel();

    public onSubmit() {
        this.validateViewModel();
        this._showError = true;
    }

    public onResetError() {
        this._showError = false;
    }
}
