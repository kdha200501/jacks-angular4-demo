import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {Subject} from 'rxjs/Subject';

export enum EVENT {
    SUBMIT,
    RESET_ERROR
}

export abstract class SubmitObservable {
    // Private static methods

    private static bindSubjectToObservable(subject: Subject<any>): Observable<any> {
        return new Observable<any>((observer: Observer<any>) => {
            subject.subscribe(observer);
        });
    }

    constructor() {
        this._subjectSubmit = new Subject<any>();// subject allows multiple observers to subscribe
        this._observableSubmit = SubmitObservable.bindSubjectToObservable(this._subjectSubmit);// whereas an observer will only communicate with the last subscriber
    }

    // Private variables

    private _observableSubmit: Observable<any>;
    private _subjectSubmit: Subject<any>;

    // Public methods

    public submit() {
        this.priorSubmit();
        this.subjectSubmit.next(EVENT.SUBMIT);
        setTimeout(() => {
            this.postSubmit();
        }, 0);
    }

    public resetError() {
        this.subjectSubmit.next(EVENT.RESET_ERROR);
    }

    public abstract priorSubmit();
    public abstract postSubmit();

    // Accessors

    get subjectSubmit(): Subject<any> {
        return this._subjectSubmit;
    }
}
