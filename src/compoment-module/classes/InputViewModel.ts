export class InputViewModel {
    constructor() {
    }

    // Private variables

    private _input: string;

    // Accessors

    get input(): string {
        return this._input;
    }
    set input(value: string) {
        this._input = value;
    }

    get hasError(): boolean {
        return !this._input;// TODO: use lodash check type
    }
}
