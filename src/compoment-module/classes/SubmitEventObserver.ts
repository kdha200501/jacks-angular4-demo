// dependencies
import {AfterViewInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';
// classes
import {EVENT} from './SubmitObservable';

export abstract class SubmitEventObserver implements AfterViewInit, OnDestroy {
    constructor() {}

    // Private variables

    private _subscription: Subscription;

    // Private methods

    private subscribeToStateChanges() {
        this._subscription = this.observableSubject.subscribe((msg: number) => {
            switch (msg) {
                case EVENT.SUBMIT:
                    this.onSubmit();
                    break;
                case EVENT.RESET_ERROR:
                    this.onResetError();
                    break;
            }
        }, () => {}, () => {
            this._subscription.unsubscribe();
        });
    }

    abstract observableSubject: Subject<any>;
    abstract onSubmit();
    abstract onResetError();

    // Public methods

    public ngAfterViewInit() {
        this.subscribeToStateChanges();
    }

    public ngOnDestroy() {
        this._subscription.unsubscribe();
    }

}
