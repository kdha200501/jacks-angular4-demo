// dependencies
import {Component, EventEmitter, Output, Input} from '@angular/core';
import {Subject} from 'rxjs/Subject';
// classes
import {InputViewModel} from '../classes/InputViewModel';
import {ValidateOnSubmit} from '../classes/ValidateOnSubmit';

@Component({
    selector:    '[select-with-validation]',
    templateUrl: '../view/component-module/components/select-with-validation.html',
    styleUrls:   ['../css/component-module/components/select-with-validation.css']
})
export class SelectWithValidation extends ValidateOnSubmit {
    @Input() id: string;
    @Input()
    get model() {
        return this._inputViewModel.input;
    }
    @Output() modelChange = new EventEmitter();
    @Input() observableSubject: Subject<any>;
    @Output() validate: EventEmitter<InputViewModel> = new EventEmitter();

    constructor() {
        super();
        this._inputViewModel = new InputViewModel();
    }

    // Private variables

    protected _inputViewModel: InputViewModel;

    // Accessors

    set model(value: string) {
        this._inputViewModel.input = value;
        this.modelChange.emit(value);
    }

    public get inputViewModel(): string{
        return this._inputViewModel.input;
    }

    public set inputViewModel(value: string) {
        this._inputViewModel.input = value;
    }

    // Public methods
    public validateViewModel() {
        this.validate.emit(this._inputViewModel);
    }

}
