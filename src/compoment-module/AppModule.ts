// dependencies
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
// nested classes
import {InputWithValidation} from './components/InputWithValidation';
import {TextAreaWithValidation} from './components/TextAreaWithValidation';
import {SelectWithValidation} from './components/SelectWithValidation';

@NgModule({
    declarations: [
        InputWithValidation,
        TextAreaWithValidation,
        SelectWithValidation
    ],
    exports: [
        InputWithValidation,
        TextAreaWithValidation,
        SelectWithValidation
    ],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule
    ]
})
export class ComponentModule {
}
